#ifndef ENEMY_SHIP_H
#define ENEMY_SHIP_H

#define DISTANCE 200

#include <cmath>

class enemy_ship
{
public:
    enemy_ship();
    int getX(void);
    int getY(void);
    int getHP(void);
    void move(int, int, int);

private:
    int x;
    int y;
    int hp;

};

#endif // ENEMY_SHIP_H
