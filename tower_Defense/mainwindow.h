#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define PI 3.14159265
#define ship_Size 20
#define MAXHEIGHT 800
#define MAXWIDTH 1200
#define MINHEIGHT 0
#define MINWIDTH 0
#define EXTRABORDER 100
#define SPAWNCONSTANT 80
#define SHIPWIDTH 60
#define SHIPHEIGHT 90
#define SHIPMOVESPEED 3
#define ASTEROIDSIZE 100 //59
#define POWERSIZE 20
#define BULLETWIDTH 3
#define BULLETHEIGHT 7
#define BULLETMULTIPLIER 2
#define LAZERSQUARENUMBER 24
#define LAZERSQUARESIZES 15
#define ENERGYNUMBER 11
#define ASTEROIDIMGNUMBER 10
#define ENEMYSIZE 30

#include <QMainWindow>
#include <QCursor>
#include <QTimer>
#include <QPainter>
#include <QString>
#include <QMouseEvent>
#include <cmath>
#include "bullet.h"
#include "big_asteroid.h"
#include "power.h"
#include "enemy_ship.h"
#include <QtMultimedia/QtMultimedia>
#include <phonon/audiooutput.h>
#include <phonon/mediaobject.h>
#include <phonon/mediasource.h>
#include <phonon/videowidget.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *ke);
    void shipMove(void);
    void asteroidMove(void);
    void enemyMove(void);
    void powersMove(void);
    void begin(void);
    void end(void);
    void closeGame(void);
    void calculate_Score(void);
    void generate_asteroid(void);
    void generage_enemy(void);
    void resetScore(void);
    void resetTime(void);
    void generate_power(int, int);
    void calculate_ship_angle(void);
    int calculate_enemy_angle(int, int);
    void empty_Screen(void);
    void toggleFlash(void);
    void die(void);
    void gameover(void);

public slots:
    void fps();
    void bullet_SLOT();
    void fire();
    void softTimer();
    void enqueueMusic();
    
private:
    Ui::MainWindow *ui;

    typedef enum {menu,game} state_type;
    typedef enum {start,exit} menu_type;
    typedef enum {left_side,right_side,top_side,bottom_side} spawn_type;
    typedef enum {playing,over} game_type;
    state_type state;
    menu_type menu_state;
    spawn_type spawnArray[4];
    game_type game_state;

    QTimer *timer;
    QTimer *timer1;
    QTimer *timer2;
    QTimer *timer3;

    int time;
    int seconds;
    int minutes;
    int bigMin;
    int min;
    int bigSec;
    int sec;

    int angle;
    unsigned maxAsteroids;

    int score;
    int thousandScore;
    int hundredScore;
    int tenScore;
    int oneScore;

    int ship_X;
    int ship_Y;
    int mouse_X;
    int mouse_Y;

    int lazarNumber;
    int energy;
    int powerupNumber;
    int location;

    bool up;
    bool down;
    bool left;
    bool right;

    bool firing;
    bool lazaring;
    bool spawning;
    bool flashShip;

    int spawning_Counter;

    int powerRed;
    int powerBlue;
    int powerGreen;

    int lives;

    std::vector<enemy_ship> enemies;
    std::vector<bullet> bullets;
    std::vector<big_Asteroid> asteroid;
    std::vector<power> powers;
};

#endif // MAINWINDOW_H
