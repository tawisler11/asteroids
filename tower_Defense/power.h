#ifndef POWER_H
#define POWER_H

class power
{
public:
    typedef enum powerups{red,blue,green} power_type;
    power();
    power(power_type , int, int);
    power_type getPowerType(void);
    int getPowerTypeArray(void);
    int getX(void);
    int getY(void);
    void move(void);

private:
    power_type power_up;
    int x;
    int y;
};

#endif // POWER_H
