#include "enemy_ship.h"

enemy_ship::enemy_ship()
{
    x = 800;
    y = 400;
    hp = 10;
}

int enemy_ship::getX()
{
    return x;
}

int enemy_ship::getY()
{
    return y;
}

int enemy_ship::getHP()
{
    return hp;
}

void enemy_ship::move(int shipx, int shipy, int angle)
{

    double temp = sqrt(pow(shipx-x,2)+pow(shipy-y,2));
    if(temp > DISTANCE+50 || temp < DISTANCE)
    {

        if(shipx - x >= 0)
        {
            if(temp*cos(angle) < DISTANCE*cos(angle)) x--;
            if(temp*cos(angle) > (DISTANCE+50)*cos(angle)) x++;
        }
        if(shipx - x < 0)
        {
            if(temp*cos(angle) < DISTANCE*cos(angle)) x++;
            if(temp*cos(angle) > (DISTANCE+50)*cos(angle)) x--;
        }

        if(shipy - y >= 0)
        {
            if(temp*sin(angle) < DISTANCE*sin(angle)) y++;
            if(temp*sin(angle) > (DISTANCE+50)*sin(angle)) y--;
        }
        if(shipy - y < 0)
        {
            if(temp*sin(angle) < DISTANCE*sin(angle)) y--;
            if(temp*sin(angle) > (DISTANCE+50)*sin(angle)) y++;
        }
    }
}


