#-------------------------------------------------
#
# Project created by QtCreator 2013-10-24T20:17:22
#
#-------------------------------------------------

QT       += core gui\
         phonon \
         network

TARGET = tower_Defense
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    bullet.cpp \
    big_asteroid.cpp \
    power.cpp \
    enemy_ship.cpp

HEADERS  += mainwindow.h \
    bullet.h \
    big_asteroid.h \
    power.h \
    enemy_ship.h

FORMS    += mainwindow.ui
