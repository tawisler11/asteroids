#include "big_asteroid.h"

big_Asteroid::big_Asteroid()
{
}

big_Asteroid::big_Asteroid(int x, int y, int hp, int speed_x, int speed_y, bool x_or_y, int angle, int rotation_speed)
{
    /*          if x_or_y is true then asteroid spawn on left or right side
                if x_or_y is false then asteroid spawn on top or bottom side
    */

    this->x = x;
    this->y = y;
    this->hp = hp;
    this->angle = angle;
    this->rotation_speed = rotation_speed;
    this->speed_x = speed_x;
    this->speed_y = speed_y;

    if(x_or_y)
    {
        in_Play_Y = true;
        in_Play_X = false;
    }
    else
    {
        in_Play_X = true;
        in_Play_Y = false;
    }
}

int big_Asteroid::getX()
{
    return x;
}

int big_Asteroid::getY()
{
    return y;
}

int big_Asteroid::getHP()
{
    return hp;
}

int big_Asteroid::getAngle()
{
    return angle;
}

void big_Asteroid::move()
{
    if(in_Play_X)
    {
        if(x > MAXWIDTH - SIZE + 1 || x < MINWIDTH + 1)
        {
            bounce_Wall(0);
        }
        if(!in_Play_Y && y < MAXHEIGHT-SIZE && y > MINHEIGHT + SIZE)
        {
            in_Play_Y = true;
        }
    }
    if(in_Play_Y)
    {
        if(y > MAXHEIGHT - SIZE + 1|| y < MINHEIGHT + 1)
        {
            bounce_Wall(1);
        }
        if(!in_Play_X && x < MAXWIDTH-SIZE && x > MINWIDTH + SIZE)
        {
            in_Play_X = true;
        }
    }

    x += speed_x;
    y += speed_y;

    rotate_asteroid();
}
void big_Asteroid::rotate_asteroid()
{
    angle = angle + rotation_speed;
    if(angle >= 360)
    {
        angle = 0;
    }
}
void big_Asteroid::takeDamage()
{
    hp = hp - 1;
}

void big_Asteroid::bounce_Wall(int wall)
{
    if(wall == 0)
    {
        speed_x = -speed_x;
    }
    if(wall == 1)
    {
        speed_y = -speed_y;
    }
}

