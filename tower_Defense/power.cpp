#include "power.h"

power::power()
{
    x = 0;
    y = 0;
    power_up = red;
}

power::power(power::power_type power_up, int x, int y)
{
    this->power_up = power_up;
    this->x = x;
    this->y = y;
}

power::power_type power::getPowerType()
{
    return power_up;
}

int power::getPowerTypeArray()
{
    if(power_up == red)
    {
        return 0;
    }
    else if(power_up == blue)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

int power::getX()
{
    return x;
}

int power::getY()
{
    return y;
}

void power::move()
{
    x++;
}
