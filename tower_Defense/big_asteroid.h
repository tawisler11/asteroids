#ifndef BIG_ASTEROID_H
#define BIG_ASTEROID_H

#include <cmath>
#define MAXHEIGHT 800
#define MAXWIDTH 1200
#define MINHEIGHT 0
#define MINWIDTH 0
#define SIZE 100

class big_Asteroid
{
public:
    big_Asteroid();
    big_Asteroid(int,int,int,int,int,bool,int,int);
    int getX(void);
    int getY(void);
    int getHP(void);
    int getAngle(void);
    void move(void);
    void takeDamage(void);
    void bounce_Wall(int);
    void rotate_asteroid(void);

private:
    int x;
    int y;
    int hp;
    int angle;
    int rotation_speed;
    int speed;
    int speed_x;
    int speed_y;
    bool in_Play_Y;
    bool in_Play_X;
};

#endif // BIG_ASTEROID_H
