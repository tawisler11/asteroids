#include "mainwindow.h"
#include "ui_mainwindow.h"

/*
 ***********************************************************************************************
 *                                  Picture Locations
 ***********************************************************************************************
 */
//==============================
//          Numbers
//==============================
QString zero = "..\\pictures\\0.png";
QString one = "..\\pictures\\1.png";
QString two = "..\\pictures\\2.png";
QString three = "..\\pictures\\3.png";
QString four = "..\\pictures\\4.png";
QString five = "..\\pictures\\5.png";
QString six = "..\\pictures\\6.png";
QString seven = "..\\pictures\\7.png";
QString eight = "..\\pictures\\8.png";
QString nine = "..\\pictures\\9.png";
QString colon = "..\\pictures\\colon.png";

//==============================
//        Title Screen
//==============================
QString exit_img = "..\\pictures\\Exit.png";
QString exit_select = "..\\pictures\\Exit_select.png";
QString play_img = "..\\pictures\\Play.png";
QString play_select = "..\\pictures\\Play_select.png";
QString title = "..\\pictures\\Title.png";

//==============================
//         Score and Time
//==============================
QString flash = "..\\pictures\\flash.png";
QString score_img = "..\\pictures\\Score.png";
QString time_img = "..\\pictures\\Time.png";

//==============================
//          Ship
//==============================
QString Ship_img = "..\\pictures\\ship.png";
QString Ship_move_img = "..\\pictures\\shipMove.png";
QString Ship_Left_Tilt_Moving_img = "..\\pictures\\ship_Left_Tilt.png";
QString Ship_Right_Tilt_Moving_img = "..\\pictures\\ship_Right_Tilt.png";

QString enemyShip = "..\\pictures\\enemyShip.png";
QString enemyShipLeft = "..\\pictures\\enemyShipLeft.png";
QString enemyShipRight = "..\\pictures\\enemyShipRight.png";

//==============================
//          Weapons
//==============================
QString bullet_img = "..\\pictures\\bullet1.png";
QString lazer_img1 = "..\\pictures\\lazar1.png";
QString lazer_img2 = "..\\pictures\\lazar2.png";
QString energy_full_img = "..\\pictures\\energy_full.png";
QString energy_empty_img = "..\\pictures\\energy_empty.png";
QString energy1_img = "..\\pictures\\energy1.png";
QString energy2_img = "..\\pictures\\energy2.png";
QString energy3_img = "..\\pictures\\energy3.png";
QString energy4_img = "..\\pictures\\energy4.png";
QString energy5_img = "..\\pictures\\energy5.png";
QString energy6_img = "..\\pictures\\energy6.png";
QString energy7_img = "..\\pictures\\energy7.png";
QString energy8_img = "..\\pictures\\energy8.png";
QString energy9_img = "..\\pictures\\energy9.png";



//==============================
//          Enemies
//==============================
QString asteroid_img = "..\\pictures\\asteroid.png";
QString asteroid1_img = "..\\pictures\\asteroid1.png";
QString asteroid2_img = "..\\pictures\\asteroid2.png";
QString asteroid3_img = "..\\pictures\\asteroid3.png";
QString asteroid4_img = "..\\pictures\\asteroid4.png";
QString asteroid5_img = "..\\pictures\\asteroid5.png";
QString asteroid6_img = "..\\pictures\\asteroid6.png";
QString asteroid7_img = "..\\pictures\\asteroid7.png";
QString asteroid8_img = "..\\pictures\\asteroid8.png";
QString asteroid9_img = "..\\pictures\\asteroid9.png";

//==============================
//          PowerUps
//==============================
QString bluePower_img = "..\\pictures\\bluePower.png";
QString bluePower1_img = "..\\pictures\\bluePower1.png";
QString bluePower2_img = "..\\pictures\\bluePower2.png";
QString redPower_img = "..\\pictures\\redPower.png";
QString redPower1_img = "..\\pictures\\redPower1.png";
QString redPower2_img = "..\\pictures\\redPower2.png";
QString greenPower_img = "..\\pictures\\greenPower.png";
QString greenPower1_img = "..\\pictures\\greenPower1.png";
QString greenPower2_img = "..\\pictures\\greenPower2.png";

//==============================
//         Backgrounds
//==============================
QString background_img = "..\\pictures\\background.png";
QString background1_img = "..\\pictures\\space_stars_background.png";
QString gameOver_img = "..\\pictures\\gameOver.png";

//==============================
//          Misc
//==============================
QString selected_Ship;

//==============================
//    Special String Arrays
//==============================
QString AsteroidArray[ASTEROIDIMGNUMBER] =
{
    asteroid9_img,
    asteroid8_img,
    asteroid7_img,
    asteroid6_img,
    asteroid5_img,
    asteroid4_img,
    asteroid3_img,
    asteroid2_img,
    asteroid1_img,
    asteroid_img
};

QString Energy_Bar[ENERGYNUMBER] =
{
    energy_empty_img,
    energy9_img,
    energy8_img,
    energy7_img,
    energy6_img,
    energy5_img,
    energy4_img,
    energy3_img,
    energy2_img,
    energy1_img,
    energy_full_img
};

QString lazerArray[2] =
{
    lazer_img1,
    lazer_img2
};

QString shipArray[2] =
{
    Ship_img,
    Ship_move_img
};

QString numbers[10] =
{
    zero,
    one,
    two,
    three,
    four,
    five,
    six,
    seven,
    eight,
    nine
};

QString powerImgArray[3][3] =
{
    {redPower_img,redPower1_img,redPower2_img},
    {bluePower_img,bluePower1_img,bluePower2_img},
    {greenPower_img,greenPower1_img,greenPower2_img}
};


/*
 ***********************************************************************************************
 *                              QRect Declarations
 ***********************************************************************************************
 */
//=============================
//          Time
//=============================
QRect tenMinRect(350,40,35,60);
QRect oneMinRect(380,40,35,60);
QRect colonRect(410,40,35,60);
QRect tenSecRect(440,40,35,60);
QRect oneSecRect(470,40,35,60);
QRect timeRect(390,15,62,25);

//=============================
//          Score
//=============================
QRect thousandRect(650,40,35,60);
QRect hundredRect(680,40,35,60);
QRect tenRect(710,40,35,60);
QRect oneRect(740,40,35,60);
QRect scoreRect(680,15,62,25);

//=============================
//      Blue Power Score
//=============================
QRect blueSymbol(50,25,30,30);
QRect bigBlueRect(50,65,17,30);
QRect blueRect(67,65,17,30);

//=============================
//      Red Power Score
//=============================
QRect redSymbol(150,25,30,30);
QRect bigRedRect(150,65,17,30);
QRect redRect(167,65,17,30);

//=============================
//      Green Power Score
//=============================
QRect greenSymbol(250,25,30,30);
QRect bigGreenRect(250,65,17,30);
QRect greenRect(267,65,17,30);

//=============================
//          Enemies
//=============================
QRect asteroidRect(0,0,ASTEROIDSIZE,ASTEROIDSIZE);

//=============================
//          Other
//=============================
QRect gameOver_Rect(480,320,240,160);
QRect backround_Rect(0,0,1200,800);
QRect playRect(500,300,200,150);
QRect exitRect(500,500,200,150);
QRect titleRect(400,50,400,100);
QRect tempRect(-SHIPWIDTH/2,-SHIPHEIGHT/2,60,90);
QRect lazarRect(-31*2,-200*2,58*2,185*2);//-31,-205.......58,185
QRect EnergyBarRect(5,785,100,10);
QRect live_ten_Rect(1000,40,35,60);
QRect live_one_Rect(1030,40,35,60);


/*
 ***********************************************************************************************
 *                              Other Globals
 ***********************************************************************************************
 */

power::power_type powerArray[3] =
{
    power::red,
    power::blue,
    power::green
};

double lazerLengths[LAZERSQUARENUMBER] =
{
    370,
    355,
    340,
    325,
    310,
    295,
    280,
    265,
    250,
    235,
    220,
    205,
    190,
    175,
    160,
    145,
    130,
    115,
    100,
    85,
    70,
    55,
    40,
    25
};

Phonon::MediaObject *music;

/*
 ***********************************************************************************************
 *                             MainWindow INIT
 ***********************************************************************************************
 */

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //QSound::play("C:\\Users\\Wiskers\\Music\\MP3 Rocket\\Dev - In The Dark - Youtube.mp3");

    music = Phonon::createPlayer(Phonon::MusicCategory, Phonon::MediaSource("C:\\Users\\Wiskers\\Music\\MP3 Rocket\\Dev - In The Dark - Youtube.mp3"));

    QUrl url("C:\\Users\\Wiskers\\Music\\MP3 Rocket\\Dev - In The Dark - Youtube.mp3");
    music->setCurrentSource(url);
    //music->setCurrentSource(Phonon::MediaSource("..\\music\\Martin Solveig & Dragonette - Hello (Out Now) - YouTube.mp3"));
    //music->enqueue(Phonon::MediaSource("C:\\Users\\Wiskers\\Music\\MP3 Rocket\\Martin Solveig & Dragonette - Hello (Out Now) - YouTube.mp3"));
    //music->play();

    spawnArray[0] = left_side;
    spawnArray[1] = right_side;
    spawnArray[2] = top_side;
    spawnArray[3] = bottom_side;

    lazarNumber = 0;
    maxAsteroids = 10;
    powerupNumber = 0;

    powerRed = 0;
    powerBlue = 0;
    powerGreen = 0;

    mouse_X = 0;
    mouse_Y = 0;
    ship_X = 600;
    ship_Y = 400;

    seconds = 0;
    minutes = 0;
    bigMin = 0;
    min = 0;
    bigSec = 0;
    sec = 0;

    spawning_Counter = 0;

    score = 0;
    thousandScore = 0;
    hundredScore = 0;
    tenScore = 0;
    oneScore = 0;

    location = 0;
    lives = 3;
    energy = ENERGYNUMBER-1;

    game_state = over;
    state = menu;
    menu_state = start;

    flashShip = false;
    spawning = true;
    firing = false;
    lazaring = false;
    up = false;
    down = false;
    left = false;
    right = false;

    generage_enemy();

    timer = new QTimer();
    timer1 = new QTimer();
    timer2 = new QTimer();
    timer3 = new QTimer();
    timer3->start(1000);
    timer2->start(100);
    timer1->start(20);
    timer->start(1000/60);
    connect(timer, SIGNAL(timeout()), this, SLOT(fps()));
    connect(timer1, SIGNAL(timeout()), this, SLOT(bullet_SLOT()));
    connect(timer2,SIGNAL(timeout()), this, SLOT(fire()));
    connect(timer3,SIGNAL(timeout()), this, SLOT(softTimer()));
    //connect(music,SIGNAL(aboutToFinish()),this, SLOT());
}

MainWindow::~MainWindow()
{
    delete ui;
}
/*
 ***********************************************************************************************
 *                                  Paint method
 ***********************************************************************************************
 */
void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter paint(this);
    QPixmap qp;

    if(state == menu)
    {
        qp.load(background_img);
        paint.drawPixmap(backround_Rect,qp);
        qp.load(title);
        paint.drawPixmap(titleRect,qp);
        if(menu_state == start)
        {
            qp.load(play_select);
            paint.drawPixmap(playRect,qp);
            qp.load(exit_img);
            paint.drawPixmap(exitRect,qp);
        }
        if(menu_state == exit)
        {
            qp.load(play_img);
            paint.drawPixmap(playRect,qp);
            qp.load(exit_select);
            paint.drawPixmap(exitRect,qp);
        }
    }
    if(state == game)
    {
        QRect tempBackground(location-MAXWIDTH,0,MAXWIDTH*2,MAXHEIGHT);

        qp.load(background1_img);
        paint.drawPixmap(tempBackground,qp);

        paint.translate(ship_X+(SHIPWIDTH/2),ship_Y+(SHIPHEIGHT/2));
        paint.rotate(angle);

        qp.load(selected_Ship);

        if(!flashShip && game_state == playing)
            paint.drawPixmap(tempRect,qp);

        if(lazaring)
        {
            qp.load(lazerArray[lazarNumber]);
            paint.drawPixmap(lazarRect,qp);//185
        }

        paint.resetTransform();

        for(unsigned i = 0; i < enemies.size(); i++)
        {
            qp.load(enemyShip);
            paint.translate(enemies.at(i).getX()+(ENEMYSIZE/2),enemies.at(i).getY()+(ENEMYSIZE/2));
            paint.rotate(calculate_enemy_angle(enemies.at(i).getX(),enemies.at(i).getY()));
            QRect tempEnemy(-(ENEMYSIZE/2),-(ENEMYSIZE/2),ENEMYSIZE,ENEMYSIZE);
            paint.drawPixmap(tempEnemy,qp);
            paint.resetTransform();
        }

        for(unsigned i = 0; i < powers.size(); i++)
        {
            qp.load(powerImgArray[powers.at(i).getPowerTypeArray()][powerupNumber]);
            QRect tempPower(powers.at(i).getX(),powers.at(i).getY(),POWERSIZE,POWERSIZE);
            paint.drawPixmap(tempPower,qp);
        }
        for(unsigned i = 0; i < asteroid.size(); i++)
        {
            qp.load(AsteroidArray[asteroid.at(i).getHP()-1]);
            paint.translate(asteroid.at(i).getX()+(ASTEROIDSIZE/2),asteroid.at(i).getY()+(ASTEROIDSIZE/2));
            paint.rotate(asteroid.at(i).getAngle());
            QRect tempAsteroid(-(ASTEROIDSIZE/2),-(ASTEROIDSIZE/2),ASTEROIDSIZE,ASTEROIDSIZE);
            paint.drawPixmap(tempAsteroid,qp);
            paint.resetTransform();
        }
        qp.load(bullet_img);
        for(unsigned i = 0; i < bullets.size(); i++)
        {
            paint.translate(bullets.at(i).getX()+(BULLETWIDTH*BULLETMULTIPLIER/2),bullets.at(i).getY()+(BULLETHEIGHT*BULLETMULTIPLIER/2));
            paint.rotate(bullets.at(i).getAngle());
            QRect bulletRect(-(BULLETWIDTH*BULLETMULTIPLIER/2),-(BULLETHEIGHT*BULLETMULTIPLIER/2),BULLETWIDTH*BULLETMULTIPLIER,BULLETHEIGHT*BULLETMULTIPLIER);
            paint.drawPixmap(bulletRect,qp);
            paint.resetTransform();
        }

        if(lives <= 4)
        {
            for(int i = 0; i < lives; i++)
            {
                QRect tempLives(900 + (SHIPWIDTH*i),30,SHIPWIDTH,SHIPHEIGHT);
                qp.load(Ship_img);
                paint.drawPixmap(tempLives,qp);
            }
        }
        else
        {
            QRect tempLives(950,25,SHIPWIDTH,SHIPHEIGHT);
            qp.load(Ship_img);
            paint.drawPixmap(tempLives,qp);
            qp.load(numbers[lives/10]);
            paint.drawPixmap(live_ten_Rect,qp);
            qp.load(numbers[lives%10]);
            paint.drawPixmap(live_one_Rect,qp);
        }

//====================================
//            Powers
//====================================
        qp.load(Energy_Bar[energy]);
        paint.drawPixmap(EnergyBarRect,qp);

//====================================
//            Powers
//====================================
        qp.load(powerImgArray[1][0]);
        paint.drawPixmap(blueSymbol,qp);
        qp.load(numbers[powerBlue/10]);
        paint.drawPixmap(bigBlueRect,qp);
        qp.load(numbers[powerBlue%10]);
        paint.drawPixmap(blueRect,qp);
        qp.load(powerImgArray[0][0]);
        paint.drawPixmap(redSymbol,qp);
        qp.load(numbers[powerRed/10]);
        paint.drawPixmap(bigRedRect,qp);
        qp.load(numbers[powerRed%10]);
        paint.drawPixmap(redRect,qp);
        qp.load(powerImgArray[2][0]);
        paint.drawPixmap(greenSymbol,qp);
        qp.load(numbers[powerGreen/10]);
        paint.drawPixmap(bigGreenRect,qp);
        qp.load(numbers[powerGreen%10]);
        paint.drawPixmap(greenRect,qp);
//=====================================
//            PRINT TIME
//=====================================
        qp.load(time_img);
        paint.drawPixmap(timeRect,qp);
        qp.load(numbers[bigMin]);
        paint.drawPixmap(tenMinRect,qp);
        qp.load(numbers[min]);
        paint.drawPixmap(oneMinRect,qp);
        qp.load(colon);
        paint.drawPixmap(colonRect,colon);
        qp.load(numbers[bigSec]);
        paint.drawPixmap(tenSecRect,qp);
        qp.load(numbers[sec]);
        paint.drawPixmap(oneSecRect,qp);

//=====================================
//            PRINT SCORE
//=====================================
        qp.load(score_img);
        paint.drawPixmap(scoreRect,score_img);
        qp.load(numbers[thousandScore]);
        paint.drawPixmap(thousandRect,qp);
        qp.load(numbers[hundredScore]);
        paint.drawPixmap(hundredRect,qp);
        qp.load(numbers[tenScore]);
        paint.drawPixmap(tenRect,qp);
        qp.load(numbers[oneScore]);
        paint.drawPixmap(oneRect,qp);

        if(game_state == over)
        {
            qp.load(gameOver_img);
            paint.drawPixmap(gameOver_Rect,qp);
        }
    }
}


/*
 ***********************************************************************************************
 *                                      Input methods
 ***********************************************************************************************
 */
void MainWindow::mousePressEvent(QMouseEvent *me)
{
    if(state == game && game_state == playing)
    {
        if(me->button() == Qt::LeftButton)
        {
            firing = true;
        }
        if(me->button() == Qt::RightButton && energy != 0)
        {
            lazaring = true;
        }
    }

    if(state == menu)
    {
        if(menu_state == start && playRect.contains(mapFromGlobal(QCursor::pos())))
        {
            begin();
        }
        if(menu_state == exit && exitRect.contains(mapFromGlobal(QCursor::pos())))
        {
            closeGame();
        }
    }
}
void MainWindow::mouseReleaseEvent(QMouseEvent *me)
{
    if(state == game)
    {
    firing = false;
    lazaring = false;
    }
}
void MainWindow::keyPressEvent(QKeyEvent *ke)
{
    if(state == menu)
    {
        if(ke->key() == Qt::Key_W || ke->key() == Qt::Key_Up)
        {
            menu_state = start;
        }
        if(ke->key() == Qt::Key_S || ke->key() == Qt::Key_Down)
        {
            menu_state = exit;
        }
        if(ke->key() == Qt::Key_Enter || ke->key() == Qt::Key_Return)
        {
            if(menu_state == start)
            {
                begin();
            }
            if(menu_state == exit)
            {
                closeGame();
            }
        }
    }

    if(state == game && game_state == over) end();

    if(state == game && game_state == playing)
    {
        if(ke->key() == Qt::Key_W)
        {
            up = true;
        }
        if(ke->key() == Qt::Key_S)
        {
            down = true;
        }
        if(ke->key() == Qt::Key_A)
        {
            left = true;
        }
        if(ke->key() == Qt::Key_D)
        {
            right = true;
        }
    }
}
void MainWindow::keyReleaseEvent(QKeyEvent *ke)
{
    if(ke->key() == Qt::Key_W)
    {
        up = false;
    }
    if(ke->key() == Qt::Key_S)
    {
        down = false;
    }
    if(ke->key() == Qt::Key_A)
    {
        left = false;
    }
    if(ke->key() == Qt::Key_D)
    {
        right = false;
    }
}
/*
 ***********************************************************************************************
 *                                              Slots
 ***********************************************************************************************
 */
void MainWindow::fire()
{
    if(lazaring && energy > 0) energy--;
    if(lazaring && energy == 0) lazaring = false;

    powerupNumber++;
    if(powerupNumber == 3) powerupNumber = 0;

    if(spawning) toggleFlash();

    if(lazaring)
    {
        lazarNumber++;
        if(lazarNumber == 2) lazarNumber=0;
    }
    if(firing == true && state == game)
    {
        bullets.push_back(bullet((ship_X)+(36*sin(angle*PI/180))+(SHIPWIDTH/2),ship_Y-(36*cos(angle*PI/180))+(SHIPHEIGHT/2)-BULLETWIDTH,atan2((double)mapFromGlobal(QCursor::pos()).y()-(ship_Y+(SHIPHEIGHT/2)),(double)mapFromGlobal(QCursor::pos()).x()-(ship_X+(SHIPWIDTH/2))),angle));
    }
}
void MainWindow::softTimer()
{
    if(state == game && game_state != over)
    {
        if(spawning)
        {
            spawning_Counter++;
            if(spawning_Counter == 3)
            {
                spawning_Counter = 0;
                spawning = false;
                flashShip = false;
            }
        }
        time++;
        seconds++;
        if(seconds == 60)
        {
            seconds = 0;
            minutes++;
        }
        bigMin = minutes/10;
        min = minutes%10;
        bigSec = seconds/10;
        sec = seconds%10;

        if(time && !spawning)
        {
            if(asteroid.size() < maxAsteroids)
            {
               generate_asteroid();
            }
        }
    }
}

void MainWindow::enqueueMusic()
{
    //music->enqueue(Phonon::MediaSource("C:\\Users\\Wiskers\\Music\\MP3 Rocket\\Dev - In The Dark - Youtube.mp3"));
}

void MainWindow::fps()
{
    if(state == menu)
    {
        if(playRect.contains(mapFromGlobal(QCursor::pos())))
        {
            menu_state = start;
        }
        if(exitRect.contains(mapFromGlobal(QCursor::pos())))
        {
            menu_state = exit;
        }
    }
    if(state == game)
    {
        angle = (int)(atan2((double)mapFromGlobal(QCursor::pos()).y()-(ship_Y+(SHIPHEIGHT/2)),(double)mapFromGlobal(QCursor::pos()).x()-(ship_X+(SHIPWIDTH/2)))*180/PI)+90;
        shipMove();
        asteroidMove();
        enemyMove();
        calculate_ship_angle();
        powersMove();
        location++;
        if(location > 1199) location = 0;
    }
    update();
}

void MainWindow::bullet_SLOT()
{
    if(state == game)
    {

    for(unsigned i = 0; i < bullets.size(); i++)
    {
        QRect tempBulletRect(bullets.at(i).getX()+20,bullets.at(i).getY()+10,20,30);
        bullets.at(i).move();

        if(bullets.at(i).getX() >= 1200 || bullets.at(i).getX() <= 0 || bullets.at(i).getY() >= 800 || bullets.at(i).getY() <= 0)
        {
            bullets.erase(bullets.begin() + i);
        }
        if(i >= bullets.size())break;
        for(unsigned k = 0; k < asteroid.size(); k++)
        {
            QRect tempAsteroidRect(asteroid.at(k).getX()+10,asteroid.at(k).getY()+10,ASTEROIDSIZE-20,ASTEROIDSIZE-20);

            if(bullets.size() != 0 && tempBulletRect.intersects(tempAsteroidRect))
            {

                bullets.erase(bullets.begin() + i);
                asteroid.at(k).takeDamage();

            }

            if(asteroid.at(k).getHP() <= 0)
            {
                score = score + 10;
                calculate_Score();
                generate_power(asteroid.at(k).getX(),asteroid.at(k).getY());
                asteroid.erase(asteroid.begin() + k);
            }
            if(k >= asteroid.size())break;
            if(i >= bullets.size())break;
        }
    }
    }
}

/*
 ***********************************************************************************************
 *                                         Void Functions
 ***********************************************************************************************
 */
void MainWindow::begin()
{
    game_state = playing;
    state = game;
    lives = 5;
    spawning = true;
}

void MainWindow::end()
{
    state = menu;
    empty_Screen();
//============================================
//            Reset functions
//============================================
    resetScore();
    resetTime();
    location = 0;
    powerRed = 0;
    powerBlue = 0;
    powerGreen = 0;
    ship_X = MAXWIDTH/2;
    ship_Y = MAXHEIGHT/2;
}
void MainWindow::die()
{
    if(lives > 0)
    {
        lives--;
        ship_X = MAXWIDTH/2;
        ship_Y = MAXHEIGHT/2;
        spawning = true;
    }
    else gameover();
}

void MainWindow::gameover()
{
    game_state = over;
}

void MainWindow::resetScore()
{
    score = 0;
    thousandScore = 0;
    hundredScore = 0;
    tenScore = 0;
    oneScore = 0;
}

void MainWindow::resetTime()
{
    time = 0;
    seconds = 0;
    minutes = 0;
    bigMin = 0;
    min = 0;
    bigSec = 0;
    sec = 0;
}

void MainWindow::closeGame()
{
    this->close();
}

void MainWindow::calculate_Score()
{
    thousandScore = score/1000;
    hundredScore = (score%1000)/100;
    tenScore = (score%100)/10 ;
    oneScore = score%10;
}

void MainWindow::generate_power(int x, int y)
{
    int temp = rand()%3;
    powers.push_back(power(powerArray[temp],x,y));
}

void MainWindow::calculate_ship_angle()
{
    if(angle < 45 && angle > -45)
    {
        if (left)
        {
            selected_Ship = Ship_Left_Tilt_Moving_img;
        }
        else if(right)
        {
            selected_Ship = Ship_Right_Tilt_Moving_img;
        }
        else if(up)
        {
            selected_Ship = Ship_move_img;
        }
        else
        {
            selected_Ship = shipArray[0];
        }
    }
    else if(angle >= 45 && angle < 135)
    {
        if (up)
        {
            selected_Ship = Ship_Left_Tilt_Moving_img;
        }
        else if(down)
        {
            selected_Ship = Ship_Right_Tilt_Moving_img;
        }
        else if(right)
        {
            selected_Ship = Ship_move_img;
        }
        else
        {
            selected_Ship = shipArray[0];
        }
    }
    else if(angle >= 135 && angle < 225)
    {
        if (left)
        {
            selected_Ship = Ship_Right_Tilt_Moving_img;
        }
        else if(right)
        {
            selected_Ship = Ship_Left_Tilt_Moving_img;
        }
        else if(down)
        {
            selected_Ship = Ship_move_img;
        }
        else
        {
            selected_Ship = shipArray[0];
        }
    }
    else
    {
        if (up)
        {
            selected_Ship = Ship_Right_Tilt_Moving_img;
        }
        else if(down)
        {
            selected_Ship = Ship_Left_Tilt_Moving_img;
        }
        else if(left)
        {
            selected_Ship = Ship_move_img;
        }
        else
        {
            selected_Ship = shipArray[0];
        }
    }
}

int MainWindow::calculate_enemy_angle(int tempX, int tempY)
{
    //angle = (int)(atan2((double)mapFromGlobal(QCursor::pos()).y()-(ship_Y+(SHIPHEIGHT/2)),(double)mapFromGlobal(QCursor::pos()).x()-(ship_X+(SHIPWIDTH/2)))*180/PI)+90;
    return ((int)(atan2(ship_Y+(SHIPHEIGHT/2)-tempY,ship_X+(SHIPWIDTH/2)-tempX)*180/PI)+90);
}

void MainWindow::empty_Screen()
{
    //============================================
    //            delete functions
    //============================================
        unsigned temp = asteroid.size();
        for(unsigned i = 0; i < temp; i++)
        {
            asteroid.erase(asteroid.begin());
        }
        temp = bullets.size();
        for(unsigned i = 0; i < temp; i++)
        {
            bullets.erase(bullets.begin());
        }
        temp = powers.size();
        for(unsigned i = 0; i < temp; i++)
        {
            powers.erase(powers.begin());
        }
}

void MainWindow::toggleFlash()
{
    if(flashShip)
        flashShip = false;
    else
        flashShip = true;
}

void MainWindow::generate_asteroid()
{
    spawn_type spawn_state;
    int temp = rand()%4;
    spawn_state = spawnArray[temp];

    int temp_X = rand()%3+1;
    int temp_Y = rand()%3+1;

    if(spawn_state == left_side)
    {
        asteroid.push_back(big_Asteroid(MINWIDTH-EXTRABORDER,rand()%(MAXHEIGHT-(SPAWNCONSTANT*2))+SPAWNCONSTANT,10,temp_X,temp_Y,true,(rand()%360+1),(rand()%3+1)));
    }
    else if(spawn_state == top_side)
    {
        asteroid.push_back(big_Asteroid(rand()%(MAXWIDTH-(SPAWNCONSTANT*2))+SPAWNCONSTANT,MINHEIGHT-EXTRABORDER,10,temp_X,temp_Y,false,rand()%360+1,rand()%3+1));
    }
    else if(spawn_state == right_side)
    {
        temp_X = -temp_X;
        asteroid.push_back(big_Asteroid(MAXWIDTH+EXTRABORDER,rand()%(MAXHEIGHT-(SPAWNCONSTANT*2))+SPAWNCONSTANT,10,temp_X,temp_Y,true,rand()%360+1,rand()%3+1));
    }
    else if(spawn_state == bottom_side)
    {
        temp_Y = -temp_Y;
        asteroid.push_back(big_Asteroid(rand()%(MAXWIDTH-(SPAWNCONSTANT*2))+SPAWNCONSTANT,MAXHEIGHT+EXTRABORDER,10,temp_X,temp_Y,false,rand()%360+1,rand()%3+1));
    }
}

void MainWindow::generage_enemy()
{
    enemies.push_back(enemy_ship());
}

void MainWindow::shipMove()
{
    if(up == true && ship_Y >= 1)
    {
        ship_Y -= SHIPMOVESPEED;
    }
    if(down == true && ship_Y <= (MAXHEIGHT - SHIPHEIGHT - 1))
    {
        ship_Y += SHIPMOVESPEED;
    }
    if(left == true && ship_X >= 1)
    {
        ship_X -= SHIPMOVESPEED;
    }
    if(right == true && ship_X <= (MAXWIDTH - SHIPWIDTH - 1))
    {
        ship_X += SHIPMOVESPEED;
    }

     QRect temp(ship_X,ship_Y,SHIPWIDTH,SHIPHEIGHT);
     for(unsigned i = 0; i < powers.size(); i++)
     {
         QRect tempPower(powers.at(i).getX(),powers.at(i).getY(),POWERSIZE,POWERSIZE);
         if(temp.intersects(tempPower))
         {
             power::power_type tempPowerType = powers.at(i).getPowerType();
             if(tempPowerType == power::red)
             {
                powerRed++;
                if(powerRed == 10)
                {
                    powerRed = 0;
                    lives++;
                }
             }
             else if(tempPowerType == power::blue)
             {
                powerBlue++;
             }
             else
             {
                powerGreen++;
                if(energy < ENERGYNUMBER-1) energy++;
             }
             powers.erase(powers.begin() + i);
         }
         if(i >= powers.size())break;
         if(powers.at(i).getX() >= 1200) powers.erase(powers.begin()+i);
         if(i >= powers.size())break;
     }
}

void MainWindow::asteroidMove()
{
    /* IN ORDER TO CHANGE THE DEATH COLLISION AREA - ADJUST "temp" */
    QRect temp(ship_X+10,ship_Y+30,SHIPWIDTH/2,SHIPHEIGHT/2);

    for(unsigned i = 0; i < asteroid.size(); i++)
    {
        QRect asteroidTemp(asteroid.at(i).getX()-10,asteroid.at(i).getY()-10,ASTEROIDSIZE-20,ASTEROIDSIZE-20);
        asteroid.at(i).move();

        if(lazaring)
        {
            for(unsigned l = 0; l < LAZERSQUARENUMBER; l++)
            {
                int tempX = ((lazerLengths[l]*cos((angle+90)*PI/180))-(LAZERSQUARESIZES/2));//lazerLengths[l]
                int tempY = ((lazerLengths[l]*sin((angle+90)*PI/180))-(LAZERSQUARESIZES/2));//lazerLengths[l]
                QRect tempLazar(ship_X-tempX,ship_Y-tempY,LAZERSQUARESIZES,LAZERSQUARESIZES);
                if(asteroidTemp.intersects(tempLazar))
                {
                    asteroid.at(i).takeDamage();
                    break;
                }
            }
        }

        if(asteroid.at(i).getHP() <= 0)
        {
            generate_power(asteroid.at(i).getX(),asteroid.at(i).getY());
            asteroid.erase(asteroid.begin() + i);
        }


        if(temp.intersects(asteroidTemp) && !spawning)
        {
            die();
        }
    }
}

void MainWindow::enemyMove()
{
    for(int i = 0; i < enemies.size(); i++)
    {
        enemies.at(i).move(ship_X, ship_Y, ((int)(atan2(ship_Y+(SHIPHEIGHT/2)-enemies.at(i).getY(),ship_X+(SHIPWIDTH/2)-enemies.at(i).getX())*180/PI)+90));
    }
}

void MainWindow::powersMove()
{
    for(unsigned i = 0; i < powers.size(); i++)
    {
        powers.at(i).move();
    }
}

