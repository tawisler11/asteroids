#ifndef BULLET_H
#define BULLET_H

#define PI 3.14159265

#include <cmath>

class bullet
{
public:
    bullet();
    bullet(int, int, double, int);
    void move(void);
    int getX(void);
    int getY(void);
    int getAngle(void);


private:
    int X;
    int Y;
    int angle;
    int speed;
    int speed_x;
    int speed_y;
};

#endif // BULLET_H
