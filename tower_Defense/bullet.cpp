#include "bullet.h"

bullet::bullet()
{
    X=0;
    Y=0;
    speed = 0;
}

bullet::bullet(int X, int Y, double angle, int angletwo)
{
    this->X = X;
    this->Y = Y;
    this->angle = angletwo;
    speed = 10;
    speed_x = (speed*cos(angle));
    speed_y = (speed*sin(angle));
}

void bullet::move()
{
    X += speed_x;
    Y += speed_y;
}

int bullet::getX()
{
    return X;
}

int bullet::getY()
{
    return Y;
}

int bullet::getAngle(void)
{
    return angle;
}
